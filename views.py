from string import ascii_uppercase
from flask import render_template, session, jsonify, make_response, request
from app import app
from DBUtils import DBUtils
from Hangman import Hangman
import logging


logger = logging.getLogger(__name__)


@app.context_processor
def symbols():
    return {'symbols': "".join(str(i) for i in xrange(10)) + ascii_uppercase}


@app.route("/", methods=["GET"])
def index():
    return render_template('index.html')


@app.route("/start_game/<username>", methods=["POST"])
def start_game(username):
    dbUtils = DBUtils()

    user = dbUtils.get_or_create_user(username)
    high_score = user.high_score or dbUtils.get_or_create_high_score(user)
    word = dbUtils.get_random_word()

    hangman = Hangman()
    hangman.set_word(word)
    session[username] = hangman.to_json()

    body = jsonify({
        'wordLength': len(word),
        'attemptsLeft': hangman.attempts_left,
        'highScore': high_score.score
    })
    response = make_response(body)
    response.set_cookie('username', username)
    return response


@app.route("/guess_symbol/<letter>", methods=["POST"])
def guess_symbol(letter):
    username = request.cookies.get('username')
    if not username:
        return make_response(("Username is not on the cookies, "
                              "please restart the page!", 400))

    dbUtils = DBUtils()

    user = dbUtils.get_user(username)
    if not user:
        return make_response(("Unknown user, please restart the page!", 400))

    hangman_json = session.get(username)
    if not hangman_json:
        return make_response(("Game is not started or already finished, "
                              "please restart page!", 400))

    hangman = Hangman()
    hangman.from_json(hangman_json)

    high_score = user.high_score
    if not high_score:
        logger.warn("Highscore for user not found, but will be created.")
        high_score = dbUtils.get_or_create_high_score(user)

    state = hangman.guess_letter(letter)
    session[username] = hangman.to_json()

    score = hangman.score

    if state in [Hangman.WON, Hangman.CONTINUE] and score > high_score.score:
        high_score = dbUtils.update_high_score(high_score, score)

    if state in [Hangman.WON, Hangman.LOST]:
        session.pop(username)

    return make_response(jsonify({
        'state': state,
        'guessedPositions': hangman.guessed_positions,
        'score': score,
        'highScore': high_score.score,
        'attemptsLeft': hangman.attempts_left
    }))
