# Hangman

Hangman is a [3DHubs](https://www.3dhubs.com/) engineering cahllenge.

Requirments:

- Chooses a random word out of 6 words: [3dhubs, marvin, print, filament, order, layer]
- Display the spaces for the letters of the word (eg: '_ _ _ _ _' for 'order')
- The user can choose a letter and if that letter exists in the chosen word it should be
shown on the puzzle (eg: asks for 'r' and now it shows '_ r _ _ r' for 'order')
- The user can only ask 5 letters that don't exist in the word and then it's game overIf the
user wins, congratulate the user and save their high score (you are free to define what is
a “high score”)

Additional requirements:

- Provide a simple API for clients to play the game
- Provide an interface for users to play the game

Used stack:

- python 2.7
- Flask, Flask-SqlAlchemy
- html, css, JavaScript/jQuery

Python static analysis:

- pycodestyle(ex. pep8) for pep8 style checking
- flake8 for static error checking
- jslint for JavaScript linting

For testing pytest was used:

- run tests in root game directory with:
  ```
  pytest ./tests/test_hangman.py
  ```

How to run locally:

- install python 2.7 or later
- open terminal and `cd` inside directory with game
- create virtualenv:
  ```
  virtualenv venv
  ```
- install dependencies:
  ```
  pip install -r requirments
  ```
- Create and prepare database with command:
  ```
  python manage.py db upgrade
  ```
- run server with:
  ```
  python run.py
  ```
- access game from you favorite browser: `localhost:5000`

How to run in virutal machine environment:

- install [Vagrant](https://www.vagrantup.com/)
- open terminal and `cd` inside directory with game
- run 
  ```vagrant up```

If you already have `docker` installed, you can skip `vagrant` steps and just use docker:

- open terminal and `cd` inside directory with game
- create docker image
  ```docker build -t hangman . ```
- start container with game
  ```docker run -p 5000:5000 --name hangman hangman```
- access game from you favorite browser: `localhost:5000`