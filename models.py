from app import db


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True, nullable=False)
    high_score = db.relationship("HighScore",
                                 uselist=False,
                                 back_populates="user")

    def __repr__(self):
        return "<User %r>" % self.username


class HighScore(db.Model):
    __tablename__ = "high_score"
    id = db.Column(db.Integer, primary_key=True)
    score = db.Column(db.Integer, default=0)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User", back_populates="high_score")

    def __repr__(self):
        return "<HighScore %r>" % self.score


class Vocabulary(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(128), unique=True, nullable=False)

    def __repr__(self):
        return "<Vocabulary %r>" % self.word
