#!/usr/bin/env python

from app import app
import views
from DBUtils import DBUtils

if __name__ == "__main__":
    dbUtils = DBUtils()
    dbUtils.fill_vocabulary_with_defaults()

    app.run(host="0.0.0.0")
