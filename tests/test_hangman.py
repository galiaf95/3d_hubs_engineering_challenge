import pytest
import os
import json
from os.path import dirname, join
import tempfile

from app import app
import views
from models import Vocabulary
from DBUtils import DBUtils

from flask_migrate import upgrade

root = dirname(dirname(__file__))
dbUtils = DBUtils()

# Hangman states of game
WON = 0
LOST = 1
CONTINUE = 2


@pytest.fixture
def client():
    db_fd, db_path = tempfile.mkstemp()

    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///{}".format(db_path)
    client = app.test_client()

    with app.app_context():
        upgrade(directory=join(root, "migrations"))
        dbUtils.get_or_insert(Vocabulary, word="test")

    yield client

    os.close(db_fd)
    os.unlink(db_path)


def test_index_page(client):
    """Test for index page"""

    rv = client.get('/')
    assert rv.status_code == 200
    assert "<title>Hangman</title>" in rv.data


def test_game_win(client):
    rv = client.post('/start_game/test')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['wordLength'] == 4
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 0

    rv = client.post('/guess_symbol/t')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert all([a == b for a, b in zip(data['guessedPositions'], [0, 3])])
    assert data['highScore'] == 2
    assert data['score'] == 2
    assert data['attemptsLeft'] == 5

    rv = client.post('/guess_symbol/e')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert all([a == b for a, b in zip(data['guessedPositions'], [1])])
    assert data['score'] == 3
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 3

    rv = client.post('/guess_symbol/s')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 0
    assert all([a == b for a, b in zip(data['guessedPositions'], [2])])
    assert data['score'] == 4
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 4

    "Checking that new game for same user will keep high scroe tracking"
    rv = client.post('/start_game/test')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['wordLength'] == 4
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 4


def test_game_lost_word_shorter_that_attempts(client):
    rv = client.post('/start_game/test')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['wordLength'] == 4
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 0

    rv = client.post('/guess_symbol/1')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 4

    rv = client.post('/guess_symbol/2')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 3

    rv = client.post('/guess_symbol/3')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 2

    rv = client.post('/guess_symbol/4')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 1
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 1


def test_game_lost_word_longer_that_attempts(client):
    db_session = dbUtils.session
    word = db_session.query(Vocabulary) \
                     .filter(Vocabulary.word == "test") \
                     .first()
    word.word = "testtest"
    db_session.commit()

    rv = client.post('/start_game/test')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['wordLength'] == 8
    assert data['attemptsLeft'] == 5
    assert data['highScore'] == 0

    rv = client.post('/guess_symbol/1')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 4

    rv = client.post('/guess_symbol/2')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 3

    rv = client.post('/guess_symbol/3')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 2

    rv = client.post('/guess_symbol/4')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 2
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 1

    rv = client.post('/guess_symbol/4')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['state'] == 1
    assert len(data['guessedPositions']) == 0
    assert data['highScore'] == 0
    assert data['score'] == 0
    assert data['attemptsLeft'] == 0
