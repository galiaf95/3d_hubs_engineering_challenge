/*jslint browser: true*/
/*global  $, alert*/
'use strict';

var heartHTML = '<i class="fas fa-heart"></i>';

var selectors = {
    startGameForm: $("#start-game-form"),
    attemptsLeft: $("#attempts-left"),
    highScore: $("#high-score"),
    score: $("#score"),
    hiddenWord: $("#hidden-word"),
    symbol: $(".symbol"),
    tryAgainBtn: $("#try-again-btn")
};

var views = {
    auth: $(".auth"),
    game: $(".game"),
    tryAgain: $(".try-again")
};

var hiddenSymbol = "_";

var states = {
    WON: 0,
    LOST: 1
};

function updateAttemptsLeft(attemptsLeft) {
    var attemptsLeftHTML = "",
        i;
    for (i = 0; i < attemptsLeft; i += 1) {
        attemptsLeftHTML += heartHTML;
    }
    selectors.attemptsLeft.html(attemptsLeftHTML);
}

function initHiddenWord(wordLength) {
    var newHiddenWord = "",
        i;
    for (i = 0; i < wordLength; i += 1) {
        newHiddenWord += hiddenSymbol;
    }
    selectors.hiddenWord.text(newHiddenWord);
}

function updateHiddenWord(symbol, guessedPositions) {
    var hiddenWord = selectors.hiddenWord.text().trim(),
        newHiddenWord = "",
        i;

    for (i = 0; i < hiddenWord.length; i += 1) {
        if (hiddenWord[i] !== hiddenSymbol) {
            newHiddenWord += hiddenWord[i];
        } else if (guessedPositions.indexOf(i) >= 0) {
            newHiddenWord += symbol;
        } else {
            newHiddenWord += hiddenSymbol;
        }
    }
    selectors.hiddenWord.text(newHiddenWord);
}

function showView(newViewName) {
    $.each(views, function (name, selector) {
        if (name === newViewName) {
            selector.removeClass("d-none");
        } else {
            selector.addClass("d-none");
        }
    });
}

function showAuthView() {
    showView('auth');
}

function activateSymbols() {
    selectors.symbol.attr("disabled", false);
}

function showGameView() {
    showView('game');
}

function showTryAgainView(success) {
    showView('tryAgain');
    var tryAgainView = views.tryAgain;
    tryAgainView.find(success ? ".won" : ".lost").removeClass("d-none");
    tryAgainView.find(!success ? ".won" : ".lost").addClass("d-none");
}

selectors.startGameForm.submit(function (e) {
    e.preventDefault();

    var username = $(this).find("#username").first().val();
    if (!username) {
        alert("Username is not valid!");
        return;
    }

    $.ajax({
        url: "/start_game/" + username,
        method: "POST"
    }).done(function (data) {
        updateAttemptsLeft(data.attemptsLeft);
        selectors.highScore.text(data.highScore);
        initHiddenWord(data.wordLength);
        showGameView();
        activateSymbols();
    }).fail(function (data) {
        console.error(data);
        alert("Something went wrong!");
    });
});

selectors.symbol.click(function () {
    var self = $(this),
        symbol = self.text().trim();
    $.ajax({
        url: "/guess_symbol/" + symbol,
        method: "POST"
    }).done(function (data) {
        updateAttemptsLeft(data.attemptsLeft);
        selectors.highScore.text(data.highScore);
        selectors.score.text(data.score);
        updateHiddenWord(symbol, data.guessedPositions);
        self.attr('disabled', true);
        var state = data.state;
        switch (state) {
        case states.WON:
            showTryAgainView(true);
            break;
        case states.LOST:
            showTryAgainView(false);
            break;
        }
    }).fail(function (data) {
        console.error(data);
        alert("Something went wrong!");
    });
});

selectors.tryAgainBtn.click(function () {
    showAuthView();
});