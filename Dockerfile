FROM python:2-alpine

WORKDIR /usr/src/hangman

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN python manage.py db upgrade

EXPOSE 5000

CMD [ "python", "./run.py" ]