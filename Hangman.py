import json


class Hangman(object):
    MAX_ATTEMPTS = 5

    WON = 0
    LOST = 1
    CONTINUE = 2

    def __init__(self):
        self.guessed_positions = []
        self.score = 0
        self.attempts_left = self.MAX_ATTEMPTS

    def set_word(self, word):
        self.word = word

    def guess_letter(self, symbol):
        word_length = len(self.word)
        old_score = self.score
        self.guessed_positions = []
        for i in xrange(word_length):
            if str(self.word[i]).lower() == str(symbol).lower():
                self.guessed_positions.append(i)
                self.score += 1

        if old_score == self.score:
            self.attempts_left -= 1

        if self.score == word_length:
            return self.WON

        if self.MAX_ATTEMPTS - self.attempts_left == word_length or self.attempts_left == 0:
            return self.LOST

        return self.CONTINUE

    def to_json(self):
        """
        Serialization function for storing data in session
        """
        return json.dumps({k: v for k, v in self.__dict__.iteritems()})

    def from_json(self, JSON):
        """
        Deserialization function for exporting data from session
        """
        for k, v in json.loads(JSON).iteritems():
            setattr(self, k, v)
