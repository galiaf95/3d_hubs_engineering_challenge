import random
from app import db
from models import Vocabulary, User, HighScore


class DBUtils(object):
    def __init__(self):
        self.session = db.session

    def get(self, model, **kwargs):
        return self.session.query(model).filter_by(**kwargs).first()

    def get_or_insert(self, model, **kwargs):
        instance = self.get(model, **kwargs)
        if instance:
            return instance
        else:
            instance = model(**kwargs)
            self.session.add(instance)
            self.session.commit()
            return instance

    def fill_vocabulary_with_defaults(self):
        words = ["3dhubs", "marvin", "print", "filament", "order", "layer"]
        for word in words:
            self.get_or_insert(Vocabulary, word=word)

    def get_user(self, name):
        return self.get(User, name=name)

    def get_or_create_user(self, name):
        return self.get_or_insert(User, name=name)

    def get_high_score(self, user):
        return user.high_score or self.get(HighScore, user=user)

    def get_or_create_high_score(self, user):
        return self.get_or_insert(HighScore, user=user, score=0)

    def get_random_word(self):
        words = self.session.query(Vocabulary.word).all()
        return random.choice(words)[0]

    def update_high_score(self, high_score, score):
        high_score.score = score
        self.session.commit()
        return high_score
